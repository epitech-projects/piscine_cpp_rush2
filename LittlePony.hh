//
// LittlePony.hh for littlepony in /home/degreg_e/rendu/piscine/piscine_cpp_rush2
//
// Made by enzo degregorio
// Login   <degreg_e@epitech.net>
//
// Started on  Sat Jan 18 14:32:08 2014 enzo degregorio
// Last update Sat Jan 18 17:26:58 2014 Jean Gravier
//

#ifndef _LITTLEPONY_H_
# define _LITTLEPONY_H_

# include "Toy.hh"
# include <string>

class		LittlePony : public Toy
{
public:
  LittlePony(std::string const&);
  ~LittlePony();
  void	isTaken() const;
};

#endif /* _LITTLEPONY_H_ */
