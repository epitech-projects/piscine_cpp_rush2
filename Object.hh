//
// Object.hh for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush2
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 18 14:37:27 2014 Jean Gravier
// Last update Sat Jan 18 15:31:31 2014 Jean Gravier
//

#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <string>

class			Object
{
public:
  Object(std::string const& = "");
  virtual ~Object();

public:
  std::string const&	getTitle() const;

protected:
  std::string		_title;
};

Object			**MyUnitTest();

#endif /* _OBJECT_H_ */
