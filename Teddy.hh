//
// Teddy.hh for teddy in /home/degreg_e/rendu/piscine/piscine_cpp_rush2
//
// Made by enzo degregorio
// Login   <degreg_e@epitech.net>
//
// Started on  Sat Jan 18 14:39:22 2014 enzo degregorio
// Last update Sat Jan 18 17:27:05 2014 Jean Gravier
//

#ifndef _TEDDY_H_
# define _TEDDY_H_

# include "Toy.hh"
# include <string>

class		Teddy : public Toy
{
public:
  Teddy(std::string const&);
  ~Teddy();
  void	isTaken() const;
};

#endif /* _TEDDY_H_ */
