//
// Object.cpp for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush2
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 18 14:37:22 2014 Jean Gravier
// Last update Sat Jan 18 14:55:39 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Object.hh"

Object::Object(std::string const& title): _title(title)
{

}

Object::~Object()
{

}

std::string const&		Object::getTitle() const
{
  return (this->_title);
}
