//
// Teddy.cpp for teddy in /home/degreg_e/rendu/piscine/piscine_cpp_rush2
//
// Made by enzo degregorio
// Login   <degreg_e@epitech.net>
//
// Started on  Sat Jan 18 15:10:43 2014 enzo degregorio
// Last update Sat Jan 18 17:28:51 2014 Jean Gravier
//

#include "Toy.hh"
#include "Object.hh"
#include "Teddy.hh"
#include <string>
#include <iostream>

Teddy::Teddy(std::string const& title): Object(title), Toy(title)
{

}

Teddy::~Teddy()
{

}

void	Teddy::isTaken() const
{
  std::cout << "gra hu" << std::endl;
}
