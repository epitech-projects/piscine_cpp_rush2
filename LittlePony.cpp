//
// LittlePony.cpp for littlepony in /home/degreg_e/rendu/piscine/piscine_cpp_rush2
//
// Made by enzo degregorio
// Login   <degreg_e@epitech.net>
//
// Started on  Sat Jan 18 14:38:48 2014 enzo degregorio
// Last update Sat Jan 18 17:31:48 2014 Jean Gravier
//

#include "Toy.hh"
#include "Object.hh"
#include "LittlePony.hh"
#include <string>
#include <iostream>

LittlePony::LittlePony(std::string const& title): Object(title), Toy(title)
{
}

LittlePony::~LittlePony()
{
}

void	LittlePony::isTaken() const
{
  std::cout << "yo man" << std::endl;
}
