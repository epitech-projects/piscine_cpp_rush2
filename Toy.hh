//
// Toy.hh for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush2
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 18 14:28:36 2014 Jean Gravier
// Last update Sat Jan 18 17:28:29 2014 Jean Gravier
//

#ifndef _TOY_H_
#define _TOY_H_

#include <string>
#include "Object.hh"

class			Toy: virtual public Object
{
public:
  Toy(std::string const&);
  virtual ~Toy();

public:
  virtual void		isTaken() const = 0;
};

#endif /* _TOY_H_ */
