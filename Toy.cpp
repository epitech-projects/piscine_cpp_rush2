//
// Toy.cpp for Rush in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush2
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 18 14:29:05 2014 Jean Gravier
// Last update Sat Jan 18 15:07:05 2014 Jean Gravier
//

#include "Toy.hh"

Toy::Toy(std::string const& title): Object(title)
{

}

Toy::~Toy()
{

}
